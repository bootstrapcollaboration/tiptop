#include "compute_new_point.hxx"

#include <fstream>
#include <limits>

namespace
{
  void write_output(const std::optional<Point> &new_point, std::ostream &os)
  {
    if(new_point.has_value())
      {
        os.precision(std::numeric_limits<double>::max_digits10);
        os << new_point.value() << "\n";
      }
    else
      {
        os << "[]\n";
      }
  }
}

void read_input(std::istream &input, std::set<Point> &feasible,
                std::set<Point> &infeasible, std::deque<Point> &in_progress,
                double &max_gap, double &resolution);

int main(int argc, char *argv[])
{
  using namespace std::string_literals;
  if(argc < 2 || argv[1] == "-h"s || argv[1] == "--help"s)
    {
      std::cerr << "Usage: tiptop <infile> <outfile>.\n"
                   "  If <infile> is a dash '-', then tiptop reads from "
                   "standard output.\n"
                   "  If <outfile> is missing or a dash '-', tiptop writes "
                   "to standard output.\n";
      exit(0);
    }

  std::set<Point> feasible, infeasible;
  std::deque<Point> in_progress;
  double max_gap, resolution;
  if(argv[1] == "-"s)
    {
      read_input(std::cin, feasible, infeasible, in_progress, max_gap,
                 resolution);
    }
  else
    {
      std::ifstream input(argv[1]);
      read_input(input, feasible, infeasible, in_progress, max_gap,
                 resolution);
    }

  // Make this a little less than 51 bits so that we do not have
  // problems with round-tripping.
  const int64_t bits_in_tree(48);
  std::optional<Point> new_point(compute_new_point(
    feasible, infeasible, in_progress, bits_in_tree, max_gap, resolution));

  if(argc > 2 && argv[2] != "-"s)
    {
      std::ofstream outfile(argv[2]);
      write_output(new_point, outfile);
    }
  else
    {
      write_output(new_point, std::cout);
    }
}
