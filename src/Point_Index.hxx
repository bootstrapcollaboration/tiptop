#pragma once

#include "Dimension.hxx"

#include <Eigen/Core>

using Point_Index = Eigen::Matrix<int64_t,dim,1>;
