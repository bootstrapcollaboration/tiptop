#pragma once

#include "Point_Index.hxx"

#include <vector>
#include <iostream>
#include <bitset>

struct Tree
{
  Point_Index lower, upper;
  std::vector<Tree> children;
  bool occupied;

  bool covers(const Point_Index &p) const
  {
    for(size_t index(0); index < dim; ++index)
      {
        if(p[index] < lower[index] || p[index] >= upper[index])
          {
            return false;
            break;
          }
      }
    return true;
  }
  void insert(const Point_Index &p);
  void create_children();
  size_t child_index(const Point_Index &p) const
  {
    std::bitset<dim> bitset(0);
    for(size_t index(0); index < dim; ++index)
      {
        bitset[index]
          = (p[index] - lower[index]) * 2 / (upper[index] - lower[index]);
      }
    return bitset.to_ullong();
  }
  int64_t
  largest_empty_level(const Point_Index &p, const int64_t &level) const;
  int64_t largest_empty_level(const Point_Index &p) const
  {
    return largest_empty_level(p, 0);
  }

  int64_t level(const Point_Index &p, const int64_t &current_level) const;
  int64_t level(const Point_Index &p) const { return level(p, 0); }

  Tree(const Point_Index &Lower, const Point_Index &Upper)
      : lower(Lower), upper(Upper), occupied(false)
  {}
};

inline std::ostream &operator<<(std::ostream &os, const Tree &tree)
{
  os << tree.lower << " " << tree.upper << " " << std::boolalpha
     << tree.occupied << " " << tree.children.size();
  return os;
}
