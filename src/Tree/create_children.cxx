#include "../Tree.hxx"

void Tree::create_children()
{
  if(children.empty())
    {
      children.reserve(1 >> dim);
      Point_Index delta((upper - lower) / 2), child_lower;
      if(dim == 2)
        {
          for(child_lower[1] = lower[1]; child_lower[1] != upper[1];
              child_lower[1] += delta[1])
            for(child_lower[0] = lower[0]; child_lower[0] != upper[0];
                child_lower[0] += delta[0])
              {
                children.emplace_back(child_lower, child_lower + delta);
              }
        }
      else
        {
          for(child_lower[2] = lower[2]; child_lower[2] != upper[2];
              child_lower[2] += delta[2])
            for(child_lower[1] = lower[1]; child_lower[1] != upper[1];
                child_lower[1] += delta[1])
              for(child_lower[0] = lower[0]; child_lower[0] != upper[0];
                  child_lower[0] += delta[0])
                {
                  children.emplace_back(child_lower, child_lower + delta);
                }
        }
    }
}
