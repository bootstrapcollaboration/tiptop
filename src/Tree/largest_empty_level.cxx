#include "../Tree.hxx"

int64_t
Tree::largest_empty_level(const Point_Index &p, const int64_t &level) const
{
  if(!occupied)
    {
      return level;
    }
  Point_Index identity;
  identity.fill(1);
  if((upper - lower) == identity)
    {
      return level + 1;
    }
  return children.at(child_index(p)).largest_empty_level(p, level + 1);
}
