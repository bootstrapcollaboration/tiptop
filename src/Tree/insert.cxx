#include "../Tree.hxx"

#include <sstream>

void Tree::insert(const Point_Index &p)
{
  if(!covers(p))
    {
      std::stringstream ss;
      ss << "INTERNAL ERROR when trying to insert a point: " << p << " "
         << *this << "\n";
      throw std::runtime_error(ss.str());
    }
  occupied = true;
  Point_Index identity;
  identity.fill(1);
  if(upper - lower != identity)
    {
      create_children();
      if(!children.empty())
        {
          children.at(child_index(p)).insert(p);
        }
    }
}
