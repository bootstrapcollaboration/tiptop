#include "../Tree.hxx"

int64_t Tree::level(const Point_Index &p, const int64_t &level) const
{
  if(!occupied)
    {
      throw std::runtime_error("INTERNAL ERROR in Tree::level: "
                               + std::to_string(level));
    }

  Point_Index identity;
  identity.fill(1);
  if(upper - lower == identity)
    {
      return 0;
    }
  int64_t num_child_occupied(0);
  for(auto &child : children)
    {
      if(child.occupied)
        {
          ++num_child_occupied;
        }
    }

  int64_t child_level(children.at(child_index(p)).level(p, level + 1));
  return (num_child_occupied > 1) ? std::max(level + 1, child_level)
                                  : child_level;
}
