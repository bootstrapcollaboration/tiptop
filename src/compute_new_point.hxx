#pragma once

#include "Point.hxx"

#include <optional>
#include <set>
#include <deque>

std::optional<Point>
compute_new_point(const std::set<Point> &feasible,
                  const std::set<Point> &infeasible,
                  const std::deque<Point> &in_progress,
                  const int64_t &bits_in_tree, const int64_t &max_gap,
                  const double &threshold);
