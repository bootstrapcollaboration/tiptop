#pragma once

#include <cstdint>

// Dimension not including the gap
constexpr int64_t dim(3);
