#pragma once

#include "Point_Index.hxx"

#include <iostream>
#include <array>

struct Point
{
  Eigen::Matrix<double, dim, 1> p;
  int64_t gap;
  Point(const Eigen::Matrix<double, dim, 1> &P, const int64_t &Gap)
      : p(P), gap(Gap)
  {}
};

inline bool operator<(const Point &a, const Point &b)
{
  if(a.gap < b.gap)
    {
      return true;
    }
  else if(a.gap > b.gap)
    {
      return false;
    }
  for(size_t index(0); index < dim; ++index)
    {
      if(a.p[index] < b.p[index])
        {
          return true;
        }
      else if(a.p[index] > b.p[index])
        {
          return false;
        }
    }
  return false;
}

inline std::ostream &operator<<(std::ostream &os, const Point &a)
{
  os << "[";
  for(size_t index(0); index<dim; ++index)
    {
      os << a.p[index] << ", ";
    }
  os << a.gap << "]";
  return os;
}
