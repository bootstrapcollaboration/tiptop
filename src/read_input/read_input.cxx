#include "../Point.hxx"

#include <boost/property_tree/json_parser.hpp>

#include <set>
#include <deque>

Point parse_point(const boost::property_tree::ptree &point);

void read_input(std::istream &input, std::set<Point> &feasible,
                std::set<Point> &infeasible, std::deque<Point> &in_progress,
                double &max_gap, double &resolution)
{
  boost::property_tree::ptree tree;
  boost::property_tree::read_json(input, tree);

  max_gap = tree.get<double>("max_gap");
  resolution = tree.get<double>("resolution", 0.5);
  for(auto &point : tree.get_child("feasible"))
    {
      feasible.emplace(parse_point(point.second));
    }
  if(tree.find("infeasible") != tree.not_found())
    {
      for(auto &point : tree.get_child("infeasible"))
        {
          infeasible.emplace(parse_point(point.second));
        }
    }
  if(tree.find("in_progress") != tree.not_found())
    {
      for(auto &point : tree.get_child("in_progress"))
        {
          in_progress.emplace_back(parse_point(point.second));
        }
    }
}
