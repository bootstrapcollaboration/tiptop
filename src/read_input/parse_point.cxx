#include "../Point.hxx"

#include <boost/property_tree/json_parser.hpp>

Point parse_point(const boost::property_tree::ptree &point)
{
  Eigen::Matrix<double, dim, 1> array;
  int64_t gap(std::numeric_limits<int64_t>::max());
  size_t index(0);
  for(auto &child : point.get_child(""))
    {
      if(index < dim)
        {
          array[index] = child.second.get_value<double>();
        }
      else
        {
          double double_gap(child.second.get_value<double>());
          gap = double_gap;
          if(gap != double_gap)
            {
              throw std::runtime_error("Gap must be an integer, but found: "
                                       + std::to_string(double_gap));
            }
        }
      ++index;
    }
  if(index != dim + 1)
    {
      throw std::runtime_error(
        "Invalid input.  Expected " + std::to_string(dim + 1)
        + " dimensions in a point, but found " + std::to_string(index));
    }
  return Point(array, gap);
}
