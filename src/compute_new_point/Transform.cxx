#include "Transform.hxx"

#include <Eigen/SVD>

Transform::Transform(const std::set<Point> &feasible,
                     const std::set<Point> &infeasible,
                     const std::deque<Point> &in_progress, const int64_t &gap,
                     const int64_t &bits_in_tree)
    : tree_extents(int64_t(1) << (bits_in_tree - 1)),
      regular_to_equalized(Eigen::Matrix<double, dim, dim>::Identity()),
      equalized_to_regular(Eigen::Matrix<double, dim, dim>::Identity())
{
  const double max_extent([&]() {
    double result(0);
    for(auto &point : feasible)
      {
        for(size_t index(0); index < dim; ++index)
          {
            result = std::max(result, abs(point.p[index]));
          }
      }
    for(auto &point : infeasible)
      {
        for(size_t index(0); index < dim; ++index)
          {
            result = std::max(result, abs(point.p[index]));
          }
      }
    for(auto &point : in_progress)
      {
        for(size_t index(0); index < dim; ++index)
          {
            result = std::max(result, abs(point.p[index]));
          }
      }
    // Add buffer (about sqrt(3)) so that corners are not rotated out of the box
    result *= 1.75;
    return result;
  }());

  // Uniformly scale by the overall extent
  regular_to_equalized *= 1 / max_extent;
  equalized_to_regular *= max_extent;

  const int64_t previous_gap([&]() {
    for(auto point(feasible.rbegin()); point != feasible.rend(); ++point)
      {
        if(point->gap != gap)
          {
            if(std::count_if(
                 point, feasible.rend(),
                 [&](const Point &p) { return p.gap == point->gap; })
               > dim + 1)
              {
                return point->gap;
              }
          }
      }
    return gap;
  }());

  if(previous_gap != gap)
    {
      Eigen::VectorXd mean_point(dim);
      int64_t num_rows(0);
      {
        Eigen::VectorXd sum(Eigen::VectorXd::Zero(dim));
        for(auto &point : feasible)
          {
            if(point.gap == previous_gap)
              {
                for(size_t index(0); index < dim; ++index)
                  {
                    sum[index] += point.p[index];
                  }
                ++num_rows;
              }
          }
        mean_point = sum / num_rows;
      }
      if(num_rows >= dim)
        {
          Eigen::MatrixXd m(num_rows, dim);
          size_t row(0);
          for(auto &point : feasible)
            {
              if(point.gap == previous_gap)
                {
                  for(size_t index(0); index < dim; ++index)
                    {
                      m(row, index) = point.p[index] - mean_point[index];
                    }
                  ++row;
                }
            }
          Eigen::JacobiSVD<Eigen::MatrixXd> svd(m, Eigen::ComputeThinU
                                                     | Eigen::ComputeThinV);

          double min_eigval(svd.singularValues().minCoeff()),
            max_eigval(svd.singularValues().maxCoeff());

          // If the ratio between eigenvalues is too large, we may
          // have a singular setup.  Then we just do uniform scaling.
          if(min_eigval > 1e-8 * max_eigval)
            {
              Eigen::DiagonalMatrix<double, dim> eigvals(svd.singularValues()
                                                         / min_eigval),
                inverse_eigvals(eigvals.inverse());

              // Convert to principle component basis and scale by
              // inverse eigenvalues
              regular_to_equalized = inverse_eigvals
                                     * svd.matrixV().transpose()
                                     * regular_to_equalized;
              equalized_to_regular *= svd.matrixV() * eigvals;
            }
        }
    }
}
