#pragma once

#include "../Point.hxx"

#include <Eigen/Core>

#include <set>
#include <deque>
#include <cmath>

struct Transform
{
  int64_t tree_extents;
  Eigen::Matrix<double, dim, dim> regular_to_equalized, equalized_to_regular;

  Transform(const std::set<Point> &feasible, const std::set<Point> &infeasible,
            const std::deque<Point> &in_progress, const int64_t &gap,
            const int64_t &bits_in_tree);

  Point_Index to_Point_Index(const Eigen::Matrix<double, dim, 1> &p) const
  {
    Eigen::Matrix<double, dim, 1> temp(regular_to_equalized * p);
    Point_Index result;
    // Write it explicitly, because Eigen does not like to mix integral and
    // floating point types
    for(size_t index(0); index < dim; ++index)
      {
        // We have to be careful with rounding, to ensure that points
        // are round-tripped correctly.
        result(index) = std::round(tree_extents * temp(index));
      }
    return result;
  }

  Eigen::Matrix<double, dim, 1> to_regular(const Point_Index &p) const
  {
    Eigen::Matrix<double, dim, 1> temp;
    for(size_t index(0); index < dim; ++index)
      {
        temp(index) = p(index);
      }
    return equalized_to_regular * (temp / tree_extents);
  }
};
