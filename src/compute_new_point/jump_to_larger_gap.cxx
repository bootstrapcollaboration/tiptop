#include "Transform.hxx"

#include <optional>

int64_t gap_ceiling(const int64_t &gap, const int64_t &max_gap,
                    const Point_Index &min, const Point_Index &max,
                    const std::set<Point> &points, const Transform &transform)
{
  int64_t result(max_gap);
  for(auto &point : points)
    {
      // Skip all of the low gap points.
      if(point.gap <= gap)
        {
          // TODO: get rid of this.  It should not matter, but it
          // might in concurrent?
          continue;
        }

      Point_Index point_index(transform.to_Point_Index(point.p));
      bool inside(true);
      // Check if the infeasible point is inside a reduced box.
      for(size_t index(0); index < dim; ++index)
        {
          inside = inside
                   && (point_index[index] > min[index]
                       && point_index[index] < max[index]);
        }

      if(inside)
        {
          result = point.gap;
          break;
        }
    }
  return result;
}

std::optional<Point>
jump_to_larger_gap(const std::set<Point> &infeasible,
                   const std::deque<Point> &in_progress,
                   const Point_Index &center, const Point_Index &dxyz,
                   const int64_t &gap, const int64_t &max_gap,
                   const Transform &transform)
{
  // Shrink the box by a factor of 2.
  const Point_Index min(center - dxyz / 2), max(center + dxyz / 2);

  // Need ordered in_progress points so that we can skip and stop
  // correctly.
  const std::set<Point> in_progress_set(in_progress.begin(),
                                        in_progress.end());

  // In progress points at larger gaps are pessimistically considered
  // infeasible
  const int64_t ceiling(
    std::min(gap_ceiling(gap, max_gap, min, max, infeasible, transform),
             gap_ceiling(gap, max_gap, min, max, in_progress_set, transform)));

  const int64_t new_gap(gap + (ceiling - gap) / 2);
  std::optional<Point> result;
  if(new_gap > gap)
    {
      result = Point(transform.to_regular(center), new_gap);
    }
  return result;
}
