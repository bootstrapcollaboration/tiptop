#include "../Point.hxx"
#include "../Tree.hxx"

#include <algorithm>
#include <optional>
#include <vector>
#include <tuple>

std::optional<Point_Index>
refine_points(const std::vector<Point_Index> &feasible_points,
              const Tree &tree, const int64_t &tree_offset,
              const Point_Index &dxyz,
              const double &resolution)
{
  if(feasible_points.empty())
    {
      throw std::runtime_error("ERROR: Need at least one feasible point");
    }
  // Get the level corresponding to the smallest box we need to resolve
  // the feasible region.
  int64_t level(0);
  const int64_t dxyz_min(dxyz.minCoeff());
  if(dxyz_min != 0)
    {
      const int64_t min_box(dxyz_min * resolution);
      int64_t ratio((2 * tree_offset) / min_box);
      std::bitset<64> bitset(static_cast<unsigned long long>(ratio));
      for(size_t pow(0); pow < bitset.size(); ++pow)
        {
          if(bitset.test(pow))
            {
              level = pow + 1;
            }
        }
    }
  else
    {
      level = tree.level(feasible_points.front()) + 1;
    }

  const int64_t box_size((2 * tree_offset) >> level);
  std::array<Point_Index, dim == 2 ? 4 : 8> delta;
  for(auto &d: delta)
    {
      d.fill(box_size);
    }
  // Feasible points are sorted.  This tries the direction
  // towards later sorted points first.  Seems to work better.
  delta[1]=-delta[0];
  delta[2][1]=-box_size;
  delta[3]=-delta[2];
  if(dim==3)
    {
      delta[4]=delta[0];
      delta[4][2]=-box_size;
      delta[5]=-delta[4];

      delta[6]=delta[2];
      delta[6][2]=-box_size;
      delta[7]=-delta[6];
    }

  int64_t current_level(level+1);
  std::optional<Point_Index> result;
  for(auto &point : feasible_points)
    {
      for(auto &d: delta)
        {
          const int64_t new_level(tree.largest_empty_level(point + d));
          if(new_level < current_level)
            {
              current_level=new_level;
              result=point+d;
            }
        }
    }
  return result;
}
