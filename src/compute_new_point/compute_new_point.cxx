#include "Transform.hxx"
#include "../Tree.hxx"

#include <optional>
#include <set>
#include <deque>
#include <tuple>

std::optional<Point_Index>
refine_points(const std::vector<Point_Index> &feasible_points,
              const Tree &tree, const int64_t &tree_offset,
              const Point_Index &dxyz, const double &resolution);

std::optional<Point>
jump_to_larger_gap(const std::set<Point> &infeasible,
                   const std::deque<Point> &in_progress,
                   const Point_Index &center, const Point_Index &dxyz,
                   const int64_t &gap, const int64_t &max_gap,
                   const Transform &transform);

std::optional<Point>
compute_new_point(const std::set<Point> &feasible,
                  const std::set<Point> &infeasible,
                  const std::deque<Point> &in_progress,
                  const int64_t &bits_in_tree, const int64_t &max_gap,
                  const double &resolution)
{
  if(feasible.empty())
    {
      throw std::runtime_error(
        "ERROR: Set of feasible points must be non-empty");
    }
  const int64_t gap(feasible.rbegin()->gap);

  const Transform transform(feasible, infeasible, in_progress, gap,
                            bits_in_tree);

  Point_Index lower, upper;
  lower.fill(-transform.tree_extents);
  upper.fill(transform.tree_extents);
  Tree feasible_tree(lower, upper);
  std::vector<Point_Index> feasible_at_gap;
  for(auto &point : feasible)
    {
      if(point.gap == gap)
        {
          Point_Index tree_index(transform.to_Point_Index(point.p));
          feasible_tree.insert(tree_index);
          feasible_at_gap.push_back(tree_index);
        }
    }

  Tree complete_tree(feasible_tree);
  for(auto &point : infeasible)
    {
      if(point.gap <= gap)
        {
          Point_Index tree_index(transform.to_Point_Index(point.p));
          complete_tree.insert(tree_index);
        }
    }
  for(auto &point : in_progress)
    {
      if(point.gap <= gap)
        {
          Point_Index tree_index(transform.to_Point_Index(point.p));
          complete_tree.insert(tree_index);
        }
      // Do not add in_progress points to the feasible list.  If we
      // added them, then we end up with (infeasible) in_progress
      // points spawning more (infeasible) in_progress points.
      // However, we do stop jumps until all in_progress at that gap
      // are completed.
    }

  Point_Index min, max;
  for(size_t index(0); index < dim; ++index)
    {
      min[index] = std::numeric_limits<int64_t>::max();
      max[index] = std::numeric_limits<int64_t>::lowest();
    }
  for(auto &point : feasible_at_gap)
    {
      for(size_t index(0); index < dim; ++index)
        {
          min[index] = std::min(min[index], point[index]);
          max[index] = std::max(max[index], point[index]);
        }
    }
  Point_Index dxyz(max - min);

  std::optional<Point_Index> new_point(refine_points(
    feasible_at_gap, complete_tree, transform.tree_extents, dxyz, resolution));
  std::optional<Point> result;
  if(new_point.has_value())
    {
      result = Point(transform.to_regular(*new_point), gap);
    }
  else
    {
      result = jump_to_larger_gap(infeasible, in_progress, (min + max) / 2,
                                  dxyz, gap, max_gap, transform);
    }
  return result;
}
