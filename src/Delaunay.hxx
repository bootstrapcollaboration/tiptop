#pragma once

#include "Dimension.hxx"

#include <CGAL/Epick_d.h>
#include <CGAL/Delaunay_triangulation.h>

using Delaunay
  = CGAL::Delaunay_triangulation<CGAL::Epick_d<CGAL::Dimension_tag<dim>>>;

inline double distance2(const Delaunay::Point &a, const Delaunay::Point &b)
{
  double result(0);
  for(size_t index(0); index < dim; ++index)
    {
      result += (a[index] - b[index]) * (a[index] - b[index]);
    }
  return result;
}
