# Overview

Tiptop generates successive points for searching for the maximum of an
N-dimensional function in N+1-dimensional space.  Tiptop can be
compiled to work in any number of dimensions.  By default, it works in
3 dimensions plus the gap.

# Usage

    tiptop <infile> <outfile>
    
If `<infile>` is a dash `'-'`, then tiptop reads from standard output.
If `<outfile>` is missing or a dash `'-'`, tiptop writes to standard
output.

# Input

Tiptop must be given at least one feasible point, a cloud of
infeasible points, and a maximum gap (`max_gap`).  Tiptop uses
`max_gap` to perform binary searches for the next feasible gap, so it
is best to set this as small as possible.

In addition, you can supply a list of points that are in-progress.
For these points, it is unknown whether they are feasible or
infeasible, but, as detailed later, they do affect which points to try
next.  In-progress points can occur when there are multiple jobs
processing points, with some jobs finishing before others.  While
tiptop can work reasonably well with a moderate number of in-progress
points (e.g. 16), it works best with zero in-progress points.

You can also supply a `resolution`, which affects how thoroughly
tiptop will explore a given gap before attempting a larger gap.

The input is given as a JSON file.  There is a [JSON
schema](docs/input_schema.json) and examples in
[input.json](test/input.json) and
[input\_resolution\_2.json](test/input_resolution_2.json).

The regular dimensions are given as regular double precision numbers.
The gap is given as an integer, so that tiptop can robustly know
whether two points are at same or different gaps.  This integer can be
any signed 64 bit integer, with a range of -2^63 to 2^63-1.  Please
note that if you are converting to and from IEEE 754 double precision
numbers, they can only exactly represent integers from -2^51 to 2^51.

# Output

Tiptop will output a single JSON array.  For example, running the
[example](test/input.json)

    tiptop test/input.json

prints

    [-0.0050000000000018476, 0.0050000000000018476, 0.0050000000000018476, 50]

You can also instruct tiptop to write to a file.  If tiptop is unable
to find a new point, it will return an empty array.

    []

# Computing New Points

Tiptop starts by computing the maximum gap of the input feasible points
(`max_feasible_gap`).  Tiptop will attempt to generate a new point at
that gap.  If that gap has already been thoroughly explored, it will
attempt to jump to a larger gap.  If that fails, tiptop will return an
empty point.

## At The Same Gap

Tiptop computes a transform using a principle component analysis of
feasible points just below `max_feasible_gap`.  This transform
regularizes the points just below `max_feasible_gap` to be relatively
spherical.  This transform is applied to all points: feasible,
infeasible, and in-progress.  Assuming that the shape of the island
does not dramatically change between gaps, this should do a reasonably
good job of regularizing the points at `max_feasible_gap`.

If there are no feasible points below `max_feasible_gap`, tiptop does
not transform any points.

Tiptop creates a multilevel partition of the space using octrees.
The boundaries of these trees is determined by the maximum extents of
the transformed points.  At the lowest level (0), there is a single
box covering everything.  At level 1, there are 8 boxes, at level 2,
there are 64 boxes, etc.  This means that for every point, there is a
critical level at which it becomes the only occupant of its box.

Tiptop computes the size of the box that covers all of the feasible
points at `max_feasible_gap`.  We use the smallest component of this
size, `dxyz_min`, as an estimate of the size the feasible region.
Since the points have been regularized, this should be a reasonable
estimate for all components.

Given this `dxyz_min` and the user supplied `resolution`, tiptop
computes the minimum resolution needed to accurately resolve the box
island as `dxyz_min * resolution`.  This size is then converted into
an equivalent tree level (`level`).

`dxyz_min` can be zero if the box is zero on any dimension.  For
example, this occurs if there is only one feasible point at
`max_feasible_gap`.  In that case, `level` is set to the critical
level for that point, plus 1.

At the given `level`, tiptop looks at the boxes adjacent to feasible
points at `max_feasible_gap`.  If they are unoccupied, tiptop returns
a point centered in that box.  So a smaller `resolution` implies that
tiptop will generate more points to fill in an island at a gap.  In
practice, a `resolution` of 0.5 seems to reliably, but slowly, find
correct minima.  Larger `resolution`s (e.g. 2) may find it much
faster, but can get stuck when sliding down the side of the
feasibility surface.

The order in which tiptop checks points is significant.  Tiptop sorts
the points, first in the x-direction, then y, then z.  This puts
points in the corner, at the smallest x, y, and z, first.  For each
point, it checks all of the adjacent boxes, starting with the
direction toward larger x, y, and z first.  This creates a natural
tendency to fill in empty internal regions first.

## Jump To A Larger Gap

If all of the adjacent boxes of all feasible points at
`max_feasible_gap` are occupied, tiptop considers jumping to a higher
gap.  However, tiptop will not jump if there are any in-progress
points at `max_feasible_gap`.  In that case, it will return an empty
point.

Once tiptop decides to jump, tiptop computes the bounding box for the
feasible points at `max_feasible_gap`.  Tiptop shrinks that box by a
factor of 2 in all directions.  It examines all infeasible and
in-progress points at a jump halfway to `max_gap`.  If there are any
infeasible or in_progress points inside the shrunken box at this
candidate gap or lower, tiptop tries halving the distance again.  This
continues until tiptop finds a new gap that the shrunken box can fit
in.  Tiptop then returns the center of the shrunken box at that new
gap.

In this way, tiptop will first try too large of a jump.  The user will
evaluate that point and inform tiptop that it is infeasible.  Tiptop
will perform a successive halving of the jump until it finds a gap
where the center of the shrunken box is feasible.  At some point,
there will be no more integers between `max_feasible_gap` and the next
candidate gap.  This effectively terminates tiptop's search, and
tiptop will always return an empty point.

## Limitations

The shrunken box could have infeasible points even at
`max_feasible_gap`.  Shrinking the box by a factor of 2 helps, but it
does not guarantee that all points within the shrunken box will be
feasible.  To get around this, we could instead compute a principle
component analysis of the feasible points at `max_feasible_gap` and
use that to model the shrunken region as a ellipsoid.

Also, this strategy assumes that the center of the island does not
move very much as the gap changes.  If the island shrinks to a point
on its boundary, tiptop may struggle to follow the island.  In a
future version, it may be useful to try additional points, such as the
foci of the shrunken ellipsoid.
