#! /usr/bin/env python
# encoding: utf-8

import os

def configure(conf):
    def get_param(varname,default):
        return getattr(Options.options,varname,'')or default

    if not conf.options.vtk_incdir:
        for d in ['VTK_INCLUDE','VTK_INCLUDE_DIR','VTK_INC_DIR']:
            env_dir=os.getenv(d)
            if env_dir:
                conf.options.vtk_incdir=env_dir
                
    if not conf.options.vtk_libdir:
        for d in ['VTK_LIB','VTK_LIB_DIR']:
            env_dir=os.getenv(d)
            if env_dir:
                conf.options.vtk_incdir=env_dir

    if not conf.options.vtk_dir:
        env_dir=os.getenv('VTK_DIR')
        if env_dir:
            conf.options.vtk_incdir=env_dir
                
    # Find VTK
    if conf.options.vtk_dir:
        if not conf.options.vtk_incdir:
            conf.options.vtk_incdir=conf.options.vtk_dir + "/include"
        if not conf.options.vtk_libdir:
            conf.options.vtk_libdir=conf.options.vtk_dir + "/lib"

    if conf.options.vtk_incdir:
        vtk_incdir=conf.options.vtk_incdir.split()
    else:
        vtk_incdir=[]
    if conf.options.vtk_libdir:
        vtk_libdir=conf.options.vtk_libdir.split()
    else:
        vtk_libdir=[]

    if conf.options.vtk_libs:
        vtk_libs=conf.options.vtk_libs.split()
    else:
        vtk_libs=['vtkIOXML-6.3', 'vtkCommonCore-6.3', 'vtkCommonDataModel-6.3']

    conf.check_cxx(msg="Checking for VTK",
                   fragment="#include <vtkPoints.h>\n#include <vtkSmartPointer.h>\nint main()\n{\nvtkSmartPointer<vtkPoints> points;\n}\n",
                   includes=vtk_incdir,
                   uselib_store='vtk',
                   libpath=vtk_libdir,
                   rpath=vtk_libdir,
                   lib=vtk_libs,
                   use=['cxx14'])

def options(opt):
    vtk=opt.add_option_group('VTK Options')
    vtk.add_option('--vtk-dir',
                   help='Base directory where vtk is installed')
    vtk.add_option('--vtk-incdir',
                   help='Directory where vtk include files are installed')
    vtk.add_option('--vtk-libdir',
                   help='Directory where vtk library files are installed')
    vtk.add_option('--vtk-libs',
                   help='Names of the vtk libraries without prefix or suffix\n'
                   '(e.g. "vtkIOXML vtkCommonCore vtkCommonDataModel")')
