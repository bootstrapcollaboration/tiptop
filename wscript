import os

def options(opt):
    opt.load(['compiler_cxx','gnu_dirs','cxx17','boost','eigen','vtk'])

def configure(conf):
    conf.load(['compiler_cxx','gnu_dirs','cxx17','boost','eigen','vtk'])

def build(bld):
    default_flags=['-Wall', '-Wextra', '-O3']
    # default_flags=['-Wall', '-Wextra', '-g']
    use_packages=['cxx17','boost','eigen']

    # Library
    bld.stlib(source=['src/compute_new_point/compute_new_point.cxx',
                      'src/compute_new_point/refine_points.cxx',
                      'src/compute_new_point/jump_to_larger_gap.cxx',
                      'src/compute_new_point/Transform.cxx',
                      'src/Tree/create_children.cxx',
                      'src/Tree/largest_empty_level.cxx',
                      'src/Tree/level.cxx',
                      'src/Tree/insert.cxx'],
              target='tiptop',
              name='tiptop_st',
              cxxflags=default_flags,
              use=use_packages
              )

    
    # Main executable
    bld.program(source=['src/main.cxx',
                        'src/read_input/read_input.cxx',
                        'src/read_input/parse_point.cxx'],
                target='tiptop',
                cxxflags=default_flags,
                use=use_packages + ['tiptop_st']
                )

    bld.program(source=['test/full_solve.cxx'],
                target='full_solve_test',
                cxxflags=default_flags,
                use=use_packages + ['tiptop_st','vtk'],
                install_path=None
                )
