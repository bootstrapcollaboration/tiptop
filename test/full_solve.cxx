#include "../src/Tree.hxx"
#include "../src/Point.hxx"
#include "../src/compute_new_point.hxx"

#include <vtkCellArray.h>
#include <vtkPoints.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>

#include <fstream>
#include <random>
#include <set>
#include <deque>

void print_tree(const Tree &tree)
{
  if(tree.occupied)
    {
      std::cout << tree << "\n";
      for(auto &child : tree.children)
        {
          print_tree(child);
        }
    }
}

bool is_feasible(const Point &point, const double &gap_scaling,
                 const std::array<double, 3> &offset)
{
  const double gap(point.gap / gap_scaling);

  const double gap_max(3);
  if(gap >= gap_max)
    {
      return false;
    }
  const std::array<double, 3> g_scale({3.0, 4.0, 5.0});
  double sum(0);
  for(size_t index(0); index < dim; ++index)
    {
      const double scale(g_scale[index]
                         // * pow((gap_max - gap) / gap_max, 1));
                         * pow((gap_max - gap) / gap_max, 1 + index / 4.0));
      const double term((point.p[index] - offset[index]) / scale);
      sum += term * term;
    }
  return sum < 1.0;
}

int main(int argc, char *argv[])
{
  std::mt19937_64 gen(17);
  std::vector<std::uniform_real_distribution<>> xyz;
  xyz.emplace_back(-4, 4);
  xyz.emplace_back(-5, 5);
  xyz.emplace_back(-5, 7);
  const size_t num_start_points(100);
  int64_t top_feasible_gap(0);

  const double gap_scaling(10000000);
  const std::array<double, 3> offset({0.1, -0.3, .7});

  std::set<Point> feasible, infeasible;
  for(size_t counter(0); counter < num_start_points; ++counter)
    {
      Eigen::Matrix<double, dim, 1> p;
      for(size_t index(0); index < dim; ++index)
        {
          p[index] = xyz[index](gen);
        }
      Point point(p, top_feasible_gap);

      if(is_feasible(point, gap_scaling, offset))
        {
          feasible.emplace(point);
        }
      else
        {
          infeasible.emplace(point);
        }
    }

  std::cout << "feasible: " << feasible.size() << " "
            << "infeasible: " << infeasible.size() << "\n";

  const int64_t max_gap(3.00010 * gap_scaling);
  const double resolution(0.5);
  const int64_t num_concurrent(7);
  if(argc < 2)
    {
      throw std::runtime_error("Need prefix\n");
    }
  size_t generation(0);

  std::deque<Point> in_progress;
  std::optional<Point> new_point;
  while(top_feasible_gap < (3 * gap_scaling - 10))
    {
      const int64_t bits_in_tree(51);
      new_point = compute_new_point(feasible, infeasible, in_progress,
                                    bits_in_tree, max_gap, resolution);

      if(new_point.has_value())
        {
          in_progress.push_back(*new_point);
        }
      else if(in_progress.empty())
        {
          std::cout << "Unable to find a new point\n";
          break;
        }

      if(in_progress.size() > num_concurrent || !new_point.has_value())
        {
          Point p(in_progress.front());
          in_progress.pop_front();
          if(is_feasible(p, gap_scaling, offset))
            {
              feasible.insert(p);
              // std::cout.precision(10);
              // std::cout << "feasible: " << p << "\n";
            }
          else
            {
              infeasible.insert(p);
              // std::cout.precision(10);
              // std::cout << "infeasible: " << p << "\n";
            }
        }

      if(feasible.rbegin()->gap > top_feasible_gap)
        {
          top_feasible_gap = feasible.rbegin()->gap;
        }
      std::cout << "top_feasible_gap: " << generation << " "
                << top_feasible_gap << " " << feasible.size() << " "
                << infeasible.size() << " " << in_progress.size() << "\n";

      // {
      //   vtkSmartPointer<vtkPoints> points_feasible
      //     = vtkSmartPointer<vtkPoints>::New();
      //   vtkSmartPointer<vtkPoints> points_infeasible
      //     = vtkSmartPointer<vtkPoints>::New();
      //   for(auto &point : feasible)
      //     {
      //       std::array<double, dim> centered;
      //       double radius(0);
      //       for(size_t index(0); index < dim; ++index)
      //         {
      //           centered[index] = point.p[index] - offset[index];
      //           radius += centered[index] * centered[index];
      //         }
      //       radius = sqrt(radius);
      //       // const double scaling(4 * pow(radius, 0.1 - 1));
      //       const double scaling(1.0);

      //       std::array<double, dim> scaled;
      //       for(size_t index(0); index < dim; ++index)
      //         {
      //           scaled[index] = centered[index] * scaling + offset[index];
      //         }

      //       points_feasible->InsertNextPoint(
      //         // point.p[0], point.p[1],
      //         scaled[0], scaled[1],
      //         // scaled[2],
      //         std::log10(3 - point.gap / gap_scaling));
      //     }
      //   for(auto &point : infeasible)
      //     {
      //       std::array<double, dim> centered;
      //       double radius(0);
      //       for(size_t index(0); index < dim; ++index)
      //         {
      //           centered[index] = point.p[index] - offset[index];
      //           radius += centered[index] * centered[index];
      //         }
      //       radius = sqrt(radius);
      //       // const double scaling(4 * pow(radius, 0.1 - 1));
      //       const double scaling(1.0);

      //       std::array<double, dim> scaled;
      //       for(size_t index(0); index < dim; ++index)
      //         {
      //           scaled[index] = centered[index] * scaling + offset[index];
      //         }

      //       points_infeasible->InsertNextPoint(
      //         // point.p[0], point.p[1],
      //         scaled[0], scaled[1],
      //         // scaled[2],
      //         std::log10(
      //           3 - std::min(point.gap / gap_scaling, 3 - 0.1 / gap_scaling)));
      //     }
      //   vtkSmartPointer<vtkXMLPolyDataWriter> writer
      //     = vtkSmartPointer<vtkXMLPolyDataWriter>::New();

      //   vtkSmartPointer<vtkPolyData> polydata_feasible
      //     = vtkSmartPointer<vtkPolyData>::New();
      //   polydata_feasible->SetPoints(points_feasible);
      //   writer->SetFileName(
      //     (argv[1] + ("feasible_" + std::to_string(generation)) + ".vtp")
      //       .c_str());
      //   writer->SetInputData(polydata_feasible);

      //   writer->SetDataModeToAscii();
      //   writer->Write();

      //   vtkSmartPointer<vtkPolyData> polydata_infeasible
      //     = vtkSmartPointer<vtkPolyData>::New();
      //   polydata_infeasible->SetPoints(points_infeasible);
      //   writer->SetFileName(
      //     (argv[1] + ("infeasible_" + std::to_string(generation)) + ".vtp")
      //       .c_str());
      //   writer->SetInputData(polydata_infeasible);

      //   writer->SetDataModeToAscii();
      //   writer->Write();
      // }
      ++generation;
    }
  return 0;
}
